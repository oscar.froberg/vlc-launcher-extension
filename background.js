function ajax(method, path, data, callback) {
    var xhr = new XMLHttpRequest()
    xhr.open(method, path, true)
    xhr.setRequestHeader('Content-type', 'application/json')
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            callback(xhr.responseText)
        }
    }
    xhr.send(data)
}

function main(info) {
    if ("linkUrl" in info) {
        console.log(`Processing ${info.linkUrl}`)
        ajax('POST', 'http://192.168.0.53:4444', JSON.stringify({url: info.linkUrl}), function(response) {
            console.log(response)
        })
    }
}

chrome.contextMenus.create({title: "Play on RPi", contexts: ["link"], id: "vlclauncher"});

chrome.contextMenus.onClicked.addListener(function(info, tab) {
    if (info.menuItemId === "vlclauncher") {
        main(info)
    }
})