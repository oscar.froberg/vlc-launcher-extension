# VLC Launcher extension

Change target server IP in `background.js`, go to chrome://extensions, set to developer mode, load unpacked extension.

To be used together with [VLC Launcher server](https://gitlab.com/oscar.froberg/vlc-launcher-server).
